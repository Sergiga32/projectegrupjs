const API_URL = "http://localhost:3000/api/cp_pais/";



function presentaLista(respuesta) {
    let tbody = document.getElementById("tablaBody");
    respuesta.forEach(row => {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.country_id));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.iso2));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.short_name));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.spanish_name));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.numcode));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode("Edit"));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode("Delete"));
        tr.appendChild(td);
        tbody.appendChild(tr);

    })
}

// function processReqChange() {
//     if (req.readyState == 4 && req.status == 200) {
//         var response = JSON.parse(req.responseText);
//         console.log(req);
//         presentaLista(response);
//         loadXMLDoc(API_URL + "count", "", processReqChange_datos);


//     }
// }
// function processReqChange_datos() {
//     if (req.readyState == 4 && req.status == 200) {
//         var response = JSON.parse(req.responseText);
//         console.log(req);
//         obtenDatos(response);
//     }
// }

function obtenDatos(respuesta) {
    var registros = respuesta[0].no_of_rows;
    console.log(registros);
}

//----------------------------------------
var req; // global variable to hold request object

// function loadXMLDoc(url, params, tratamiento) {
//     let req = ajustaReq();
//     console.log(url);
//     if (req) {
//         req.onreadystatechange = tratamiento;
//         req.open("GET", url + '?' + params, true);
//         req.send(null);
//         console.log("Peticion hecha");
//         return true;
//     }
//     return false;
// }
// function ajustaReq() {
//     if (window.XMLHttpRequest) {
//         try {
//             req = new XMLHttpRequest();
//         } catch (e) {
//             req = false;
//         }
//     } else if (window.ActiveXObject) {
//         try {
//             req = new ActiveXObject("Msxml2.XMLHTTP");
//         } catch (e) {
//             try {
//                 req = new ActiveXObject("Microsoft.XMLHTTP");
//             } catch (e) {
//                 req = false;
//             }
//         }
//     }
//     return req;
// }



// loadXMLDoc(API_URL, "", processReqChange);

get(API_URL)
    .then(function (response) {
        presentaLista(JSON.parse(response));
    })
    .catch(function (error) {
        console.error(error);
    });

get(API_URL+ "count")
    .then(function (response) {
        obtenDatos(JSON.parse(response));
    })
    .catch(function (error) {
        console.error(error);
    });
